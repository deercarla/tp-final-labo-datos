---
title: 'Trabajo Práctico Final: Anthrokids'
author: 'Carla de Erausquin, L.U.: 126/18'
date: "17 de marzo, 2022"
output:
  html_notebook: default
  pdf_document: default
subtitle: Laboratorio de Datos
---

```{r}
datos <- read.csv("chicos.csv", stringsAsFactors = FALSE) 
indices <- !is.na(datos$WEIGHT) & !is.na(datos$STATURE) & !is.na(datos$SEX) & !is.na(datos$RACE) & !is.na(datos$AGE.IN.MONTHS) 
datos <- datos[indices,]
datos <-  datos[datos$STATURE > 0,]
```

```{r}
imc <- function(altura, peso){
  res <- (peso/10) / ((altura/1000)**2)
  return (res)
}

icm_kids <- imc(datos$STATURE, datos$WEIGHT)

datos["icm"] <- icm_kids

indices2 <- !is.na(datos$icm) 
datos <- datos[indices2,]

variables <- list(datos$AGE.IN.MONTHS, datos$SEX, datos$RACE)

r2 <- matrix(nrow = length(variables), ncol = length(variables))
err <- matrix(nrow = length(variables), ncol = length(variables))

for( i in 1:length(variables) ){
  for( j in 1:length(variables) ){
    m <- lm(datos$icm ~ variables[[i]] + variables[[j]])
    r2[i, j] <- summary(m)$r.squared
    err[i, j] <- summary(m)$sigma
  }
}

names <- c('Edad', 'Sexo', 'Raza')

colnames(r2) <- names
rownames(r2) <- names

colnames(err) <- names
rownames(err) <- names

cols <- colorRampPalette(c("white", "#6565bf"))

library(lattice)

par(mfrow = c(1, 2))

levelplot(
  r2,
  scales = list(x = list(rot = 90)),
  xlab = '',
  ylab = '',
  panel = function(x, y, z, ...) {
    panel.levelplot(x,y,z,...)
    panel.text(x, y, round(z,4), cex = 0.8)
  },
  col.regions = cols,
  main = 'R^2',
  aspect="fill"
)

```
